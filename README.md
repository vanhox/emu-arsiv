# emülatör-arşiv
## emülatör
### Nintendo Game Boy Advance
- [mGBA](https://mgba.io/downloads.html) (Windows/MacOS) En beğendiğim gameboy advance emülatörü.
- [VisualBoyAdvance](http://www.emulator-zone.com/doc.php/gba/vboyadvance.html) (Windows/MacOS) Bu da güzeldir.
### Nintendo DS 
- [DeSmuMe](https://desmume.org/) (Windows/MacOS) Gördüğüm ilk Nintendo DS emülatörüydü, uzun zamandır güncelleme almıyor gördüğüm kadarıyla, bir deneyin sıkıntı olursa melon'u denersiniz.
- [MelonDS](http://melonds.kuribo64.net/) (Windows)
### Super Nintendo (SNES)
- [BSNES](https://github.com/bsnes-emu/bsnes/releases) Bu da gördüğüm en eski ve en güncel snes emülatörüdür.
- [SNES9X](https://www.snes9x.com/) (Windows/MacOS)
### Nintendo 64
- [Mupen64+](https://www.mupen64plus.org/) (Windows/MacOS)
- [Project64](https://www.pj64-emu.com/) (Windows)
### Nintendo 
- [FCEUX](https://fceux.com/web/home.html) (Windows)
- [JNES](http://www.jabosoft.com/categories/1) (Windows/Android)

